/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"x6t.io/ngau/core"
)

// ListDeployments （根据命名空间返回Deployment列表）
func (s Builders) ListDeployments(ctx *gin.Context) {
	nameSpace := ctx.DefaultQuery("namespace", "default")
	list, err := s.Deployment().List(ctx, nameSpace)
	if err != nil {
		Error(ctx.Writer, http.StatusBadRequest, err.Error())
		return
	}
	encodeJSON(ctx.Writer, http.StatusOK, list)
}

// ListWatchDeployments （根据命名空间返回Deployment列表）
func (s Builders) ListWatchDeployments(ctx *gin.Context) {
	nameSpace := ctx.DefaultQuery("namespace", "default")
	list, err := s.Deployment().ListByWatch(ctx, nameSpace)
	if err != nil {
		Error(ctx.Writer, http.StatusBadRequest, err.Error())
		return
	}
	encodeJSON(ctx.Writer, http.StatusOK, list)
}

// CreateDeployment （创建简单的Deployment示例）
func (s Builders) CreateDeployment(ctx *gin.Context) {
	var req core.Deployment
	if err := ctx.Bind(&req); err != nil {
		invalidJSON(ctx.Writer)
		return
	}
	if err := s.Deployment().Create(ctx, req); err != nil {
		Error(ctx.Writer, http.StatusBadRequest, err.Error())
		return
	}
	e := struct {
		Message string
	}{
		Message: "success",
	}
	encodeJSON(ctx.Writer, http.StatusOK, e)
}
