/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"k8s.io/client-go/kubernetes"
	"x6t.io/ngau/core"
	"x6t.io/ngau/service/apis"
	"x6t.io/ngau/service/informer"
)

type Builders struct {
	kube    kubernetes.Interface
	runtime core.RuntimeStore
	dep     core.DeploymentStore
	pod     core.PodStore
	status  core.PodStatusStore
	events  core.PodEventStore
}

func NewBuilders(kube kubernetes.Interface) Builders {
	status := &apis.PodStatusService{Interface: kube}
	events := &apis.PodEventsService{Interface: kube}
	pod := apis.NewPodService(kube, status, events)
	runtime := informer.NewRuntimeStore(kube)
	return Builders{
		kube:    kube,
		dep:     apis.NewDeploymentService(kube, pod, runtime),
		pod:     pod,
		status:  status,
		runtime: runtime,
		events:  events,
	}
}

func (s Builders) Kubernetes() kubernetes.Interface {
	return s.kube
}

func (s Builders) Informer() core.RuntimeStore {
	return s.runtime
}

func (s Builders) Deployment() core.DeploymentStore {
	return s.dep
}

func (s Builders) Pod() core.PodStore {
	return s.pod
}

func (s Builders) PodStatus() core.PodStatusStore {
	return s.status
}

func (s Builders) PodEvent() core.PodEventStore {
	return s.events
}

// ErrorMessage is the error response format for all service errors
type ErrorMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
