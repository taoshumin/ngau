/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

const JSONType = "application/json"

func NewMux(service Builders) *gin.Engine {
	router := gin.Default()

	router.GET("/deployments")
	router.GET("/namespaces")
	router.GET("/pods")
	router.GET("/ingress")
	router.POST("/ingress")
	router.DELETE("/ingress")

	return router
}

func encodeJSON(w http.ResponseWriter, status int, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(v); err != nil {
		unknownErrorWithMessage(w, err)
	}
}

func invalidJSON(w http.ResponseWriter) {
	Error(w, http.StatusBadRequest, "Unpassable JSON")
}

func unknownErrorWithMessage(w http.ResponseWriter, err error) {
	Error(w, http.StatusInternalServerError, fmt.Sprintf("Unknown error: %v", err))
}

func Error(w http.ResponseWriter, code int, msg string) {
	e := ErrorMessage{
		Code:    code,
		Message: msg,
	}
	b, err := json.Marshal(e)
	if err != nil {
		code = http.StatusInternalServerError
		b = []byte(`{"code": 500, "message":"server_error"}`)
	}

	w.Header().Set("Content-Type", JSONType)
	w.WriteHeader(code)
	_, _ = w.Write(b)
}
