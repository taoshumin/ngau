/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"github.com/gorilla/websocket"
	"net/http"
)

//var upGrader = &websocket.Upgrader{
//	CheckOrigin: func(r *http.Request) bool {
//		return true
//	},
//}
//
//func (s Builders) Websocket(ctx *gin.Context) {
//	ws, err := upGrader.Upgrade(ctx.Writer, ctx.Request, nil)
//	if err != nil {
//		Error(ctx.Writer, http.StatusBadRequest, err.Error())
//		return
//	}
//	fmt.Println(ws)
//}

type WebSocketService struct {
	upgrader *websocket.Upgrader
}

func checkSameOrigin(r *http.Request) bool {
	return true
}

func NewWebSocketService() *WebSocketService {
	return &WebSocketService{
		upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin:     checkSameOrigin,
		},
	}
}

//
//func (s *WebSocketService) HanlerWebsocket((w http.ResponseWriter, r *http.Request) {
//	conn, err := s.upgrader.Upgrade(w, r, nil)
//	if err!=nil{
//
//		return
//	}
//	fmt.Println(conn)
//}
