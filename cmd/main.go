/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"x6t.io/ngau/client"
	"x6t.io/ngau/server"
)

const (
	DefaultK8sProxyHost = "http://106.12.78.172:8080"
	DefaultHttpPort     = ":9999"
)

func main01() {
	clientSet, err := client.NewProxyClientSet(DefaultK8sProxyHost)
	if err != nil {
		panic(fmt.Sprintf("k8s new proxy client set: %s", err))
	}

	builders := server.NewBuilders(clientSet)
	if err := server.NewMux(builders).
		Run(DefaultHttpPort); err != nil {
		panic(fmt.Sprintf("run hanlder server :%s", err))
	}
}

func main() {
	c, err := client.NewClientSet("/Users/facebook/go/src/x6t.io/ngau/client/config")
	if err != nil {
		panic(err)
	}

	list, err := c.CoreV1().Pods("default").
		List(context.Background(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println(list)
}
