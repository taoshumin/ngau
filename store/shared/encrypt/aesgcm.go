package encrypt

import (
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

// Aesgcm provides an encrypter that uses the aesgcm encryption
// algorithm.
type Aesgcm struct {
	block  cipher.Block
	Compat bool
}

// Encrypt encrypts the plaintext using aesgcm.
func (e *Aesgcm) Encrypt(plaintext string) ([]byte, error) {
	gcm, err := cipher.NewGCM(e.block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, []byte(plaintext), nil), nil
}

// Decrypt decrypts the ciphertext using aesgcm.
func (e *Aesgcm) Decrypt(ciphertext []byte) (string, error) {
	gcm, err := cipher.NewGCM(e.block)
	if err != nil {
		return "", err
	}

	if len(ciphertext) < gcm.NonceSize() {
		// if the decryption utility is running in compatibility
		// mode, it will return the ciphertext as plain text if
		// decryption fails. This should be used when running the
		// database in mixed-mode, where there is a mix of encrypted
		// and unencrypted content.
		if e.Compat {
			return string(ciphertext), nil
		}
		return "", errors.New("malformed ciphertext")
	}

	plaintext, err := gcm.Open(nil,
		ciphertext[:gcm.NonceSize()],
		ciphertext[gcm.NonceSize():],
		nil,
	)
	// if the decryption utility is running in compatibility
	// mode, it will return the ciphertext as plain text if
	// decryption fails. This should be used when running the
	// database in mixed-mode, where there is a mix of encrypted
	// and unencrypted content.
	if err != nil && e.Compat {
		return string(ciphertext), nil
	}
	return string(plaintext), err
}
