package user

import (
	"context"
	"x6t.io/ngau/core"
	"x6t.io/ngau/store/shared/db"
	"x6t.io/ngau/store/shared/encrypt"
)

// New returns a new UserStore.
func New(db *db.DB, enc encrypt.Encrypter) core.UserStore {
	return &userStore{db, enc}
}

type userStore struct {
	db  *db.DB
	enc encrypt.Encrypter
}

// Find returns a user from the datastore.
func (s *userStore) Find(context.Context, int64) (*core.User, error) {
	return nil, nil
}

// FindLogin returns a user from the datastore by username.
func (s *userStore) FindLogin(context.Context, string) (*core.User, error) {
	return nil, nil
}

// FindToken returns a user from the datastore by token.
func (s *userStore) FindToken(context.Context, string) (*core.User, error) {
	return nil, nil
}

// List returns a list of users from the datastore.
func (s *userStore) List(context.Context) ([]*core.User, error) {
	return nil, nil
}

// ListRange returns a range of users from the datastore.
func (s *userStore) ListRange(context.Context, core.UserParams) ([]*core.User, error) {
	return nil, nil
}

// Create persists a new user to the datastore.
func (s *userStore) Create(context.Context, *core.User) error {
	return nil
}

// Update persists an updated user to the datastore.
func (s *userStore) Update(context.Context, *core.User) error {
	return nil
}

// Delete deletes a user from the datastore.
func (s *userStore) Delete(context.Context, *core.User) error {
	return nil
}

// Count returns a count of human and machine users.
func (s *userStore) Count(context.Context) (int64, error) {
	return -1, nil
}

// CountHuman returns a count of human users.
func (s *userStore) CountHuman(context.Context) (int64, error) {
	return -1, nil
}
