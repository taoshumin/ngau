/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package informer

import "strings"

const (
	// DefaultDeploymentPrefix return default deployment's prex.
	DefaultDeploymentPrefix = "ngau:deployment"
	DefaultPodPrefix        = "ngau:pod"
	DefaultNameSpacePrefix  = "ngau:namespace"
	DefaultIngressPrefix    = "ngau:ingress"
)

// deploymentPrefix return deployment pyrex's name by namespace.
func deploymentPrefix(nameSpace string) string {
	return strings.Join([]string{DefaultDeploymentPrefix, nameSpace}, ":")
}

// podPrefix return pod pyrex's name by namespace.
func podPrefix(nameSpace string) string {
	return strings.Join([]string{DefaultPodPrefix, nameSpace}, ":")
}

// nameSpacePrefix return pod pyrex's.
func nameSpacePrefix(namespace string) string {
	return strings.Join([]string{DefaultNameSpacePrefix, namespace}, ":")
}

// eventPodMessagePrefix event pyrex's.
func eventPodMessagePrefix(ns, kind, name string) string {
	return strings.Join([]string{ns, kind, name}, ":")
}

// ingressPrefix ingress pyrex's.
func ingressPrefix(namespace string) string {
	return strings.Join([]string{DefaultIngressPrefix, namespace}, ":")
}
