/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package informer

import (
	"errors"
	"fmt"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/networking/v1beta1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"reflect"
	"sync"
	"time"
)

type runtimeStore struct {
	client    kubernetes.Interface
	informers *Informer
	listers   *Lister

	cacheStore sync.Map
	wg         sync.WaitGroup
}

func NewRuntimeStore(client kubernetes.Interface) *runtimeStore {
	store := &runtimeStore{
		client:     client,
		informers:  &Informer{},
		listers:    &Lister{},
		cacheStore: sync.Map{},
	}
	// NewSharedInformerFactory constructs a new instance of sharedInformerFactory for all namespaces.
	infFactory := informers.NewSharedInformerFactory(client, 3*time.Second)

	// NamespaceInformer provides access to a shared informer and lister for Namespace.
	store.informers.Namespace = infFactory.Core().V1().Namespaces().Informer()
	store.listers.Namespace = infFactory.Core().V1().Namespaces().Lister()

	// DeploymentInformer provides access to a shared informer and lister for Deployment.
	store.informers.Deployment = infFactory.Apps().V1().Deployments().Informer()
	store.listers.Deployment = infFactory.Apps().V1().Deployments().Lister()

	// PodInformer provides access to a shared informer and lister for Pod.
	store.informers.Pod = infFactory.Core().V1().Pods().Informer()
	store.listers.Pod = infFactory.Core().V1().Pods().Lister()

	// EventInformer provides access to a shared informer and lister for event.
	store.informers.Events = infFactory.Core().V1().Events().Informer()
	store.listers.Events = infFactory.Core().V1().Events().Lister()

	// IngressInformer provides access to a shared informer and lister for ingress.
	store.informers.Ingress = infFactory.Networking().V1beta1().Ingresses().Informer()

	// AddEventHandler adds an event handler to the shared informer using the shared informer's resync
	// period.  Events to a single handler are delivered sequentially, but there is no coordination
	// between different handlers.
	store.informers.Namespace.AddEventHandler(store.nameSpaceEventHandler())
	store.informers.Deployment.AddEventHandler(store)
	store.informers.Pod.AddEventHandler(store)
	store.informers.Events.AddEventHandler(store)
	store.informers.Ingress.AddEventHandler(store)

	store.informers.Start(wait.NeverStop)
	return store
}

func (s *runtimeStore) Open() error {
	stop := make(chan struct{})
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		s.informers.Start(stop)
	}()

	return nil
}

func (s *runtimeStore) Close() error {

	s.wg.Wait()
	return nil
}

func (s *runtimeStore) Informer() *Informer {
	return s.informers
}

func (s *runtimeStore) Lister() *Lister {
	return s.listers
}

// nameSpaceEventHandler can handler namespace notifications for events that happen to a resource.
func (s *runtimeStore) nameSpaceEventHandler() cache.ResourceEventHandler {
	return cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			ns := obj.(*corev1.Namespace)
			s.cacheStore.Store(nameSpacePrefix(ns.Name), ns)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			ns := newObj.(*corev1.Namespace)
			s.cacheStore.Store(nameSpacePrefix(ns.Name), ns)
		},
		DeleteFunc: func(obj interface{}) {
			ns := obj.(*corev1.Namespace)
			s.cacheStore.Delete(nameSpacePrefix(ns.Name))
		},
	}
}

// OnAdd ResourceEventHandler can handle notifications for events that happen to a resource.
func (s *runtimeStore) OnAdd(obj interface{}) {
	// deployment
	if dep, ok := obj.(*v1.Deployment); ok {
		if list, ok := s.cacheStore.Load(deploymentPrefix(dep.Namespace)); ok {
			list = append(list.([]*v1.Deployment), dep)
			s.cacheStore.Store(deploymentPrefix(dep.Namespace), list)
		} else {
			s.cacheStore.Store(deploymentPrefix(dep.Namespace), []*v1.Deployment{dep})
		}
	}

	// pod
	if pod, ok := obj.(*corev1.Pod); ok {
		if list, ok := s.cacheStore.Load(podPrefix(pod.Namespace)); ok {
			list = append(list.([]*corev1.Pod), pod)
			s.cacheStore.Store(podPrefix(pod.Namespace), list)
		} else {
			s.cacheStore.Store(podPrefix(pod.Namespace), []*corev1.Pod{pod})
		}
	}

	// event
	if event, ok := obj.(*corev1.Event); ok {
		if list, ok := s.cacheStore.Load(eventPodMessagePrefix(event.Namespace, event.InvolvedObject.Kind, event.InvolvedObject.Name)); ok {
			list = append(list.([]*corev1.Event), event)
			s.cacheStore.Store(eventPodMessagePrefix(event.Namespace, event.InvolvedObject.Kind, event.InvolvedObject.Name), list)
		} else {
			s.cacheStore.Store(eventPodMessagePrefix(event.Namespace, event.InvolvedObject.Kind, event.InvolvedObject.Name), []*corev1.Event{event})
		}
	}

	// ingress
	if ingress, ok := obj.(*v1beta1.Ingress); ok {
		if list, ok := s.cacheStore.Load(ingressPrefix(ingress.Namespace)); ok {
			list = append(list.([]*v1beta1.Ingress), ingress)
			s.cacheStore.Store(ingressPrefix(ingress.Namespace), list)
		} else {
			s.cacheStore.Store(ingressPrefix(ingress.Namespace), []*v1beta1.Ingress{ingress})
		}
	}

}

func (s *runtimeStore) OnUpdate(oldObj, newObj interface{}) {
	// deployment
	if dep, ok := newObj.(*v1.Deployment); ok {
		if list, ok := s.cacheStore.Load(deploymentPrefix(dep.Namespace)); ok {
			for i, deps := range list.([]*v1.Deployment) {
				if deps.Name == dep.Name {
					list.([]*v1.Deployment)[i] = deps
				}
			}
		}
	}

	// pod
	if pod, ok := newObj.(*corev1.Pod); ok {
		if list, ok := s.cacheStore.Load(podPrefix(pod.Namespace)); ok {
			for i, pods := range list.([]*corev1.Pod) {
				if pods.Name == pod.Name {
					list.([]*corev1.Pod)[i] = pod
				}
			}
		}
	}

	// event
	if event, ok := newObj.(*corev1.Event); ok {
		s.cacheStore.Store(eventPodMessagePrefix(event.Namespace, event.InvolvedObject.Kind, event.InvolvedObject.Name), event)
	}

	// ingress
	if ingress, ok := newObj.(*v1beta1.Ingress); ok {
		if list, ok := s.cacheStore.Load(ingressPrefix(ingress.Namespace)); ok {
			for i, range_pod := range list.([]*v1beta1.Ingress) {
				if range_pod.Name == ingress.Name {
					list.([]*v1beta1.Ingress)[i] = ingress
				}
			}
		}
	}
}

func (s *runtimeStore) OnDelete(obj interface{}) {
	// deployment
	if dep, ok := obj.(*v1.Deployment); ok {
		if list, ok := s.cacheStore.Load(deploymentPrefix(dep.Namespace)); ok {
			for i, deployment := range list.([]*v1.Deployment) {
				if deployment.Name == dep.Name {
					newlist := append(list.([]*v1.Deployment)[:i], list.([]*v1.Deployment)[i+1:]...)
					s.cacheStore.Store(deploymentPrefix(dep.Namespace), newlist)
					break
				}
			}
		}
	}

	// pod
	if pod, ok := obj.(*corev1.Pod); ok {
		if list, ok := s.cacheStore.Load(podPrefix(pod.Namespace)); ok {
			for i, pods := range list.([]*corev1.Pod) {
				if pods.Name == pod.Name {
					newlist := append(list.([]*corev1.Pod)[:i], list.([]*corev1.Pod)[i+1:]...)
					s.cacheStore.Store(podPrefix(pod.Namespace), newlist)
					break
				}
			}
		}
	}

	// event
	if event, ok := obj.(*corev1.Event); ok {
		s.cacheStore.Delete(eventPodMessagePrefix(event.Namespace, event.InvolvedObject.Kind, event.InvolvedObject.Name))
	}

	// ingress
	if ingress, ok := obj.(*v1beta1.Ingress); ok {
		if list, ok := s.cacheStore.Load(ingressPrefix(ingress.Namespace)); ok {
			for i, range_ingress := range list.([]*v1beta1.Ingress) {
				if range_ingress.Name == ingress.Name {
					newList := append(list.([]*v1beta1.Ingress)[:i], list.([]*v1beta1.Ingress)[i+1:]...)
					s.cacheStore.Store(ingress.Namespace, newList)
					break
				}
			}
		}
	}
}

// GetDeploymentListByNameSpace 根据namespace获取deployment列表
func (s *runtimeStore) GetDeploymentListByNameSpace(nameSpace string) ([]*v1.Deployment, error) {
	if list, ok := s.cacheStore.Load(deploymentPrefix(nameSpace)); ok {
		return list.([]*v1.Deployment), nil
	}
	return nil, errors.New("record not found")
}

// GetDeployment 根据namespace和deplyment名称获取指定deployment
func (s *runtimeStore) GetDeployment(ns, depname string) (*v1.Deployment, error) {
	if list, ok := s.cacheStore.Load(ns); ok {
		for _, item := range list.([]*v1.Deployment) {
			if item.Name == depname {
				return item, nil
			}
		}
	}
	return nil, errors.New("record not found")
}

// GetPodListByNameSpace 根据namespace获取pod列表
func (s *runtimeStore) GetPodListByNameSpace(nameSpace string) ([]*corev1.Pod, error) {
	if list, ok := s.cacheStore.Load(podPrefix(nameSpace)); ok {
		return list.([]*corev1.Pod), nil
	}
	return nil, errors.New("record not found")
}

// GetPod 根据namespace和pod的名称获取指定的pod
func (s *runtimeStore) GetPod(namespace, podname string) *corev1.Pod {
	if list, ok := s.cacheStore.Load(namespace); ok {
		for _, pod := range list.([]*corev1.Pod) {
			if pod.Name == podname {
				return pod
			}
		}
	}
	return nil
}

// GetPodListByLabel 根据label标签获取pod列表
func (s *runtimeStore) GetPodListByLabel(namespace string, labels []map[string]string) ([]*corev1.Pod, error) {
	ret := make([]*corev1.Pod, 0)
	if list, ok := s.cacheStore.Load(namespace); ok {
		for _, pod := range list.([]*corev1.Pod) {
			for _, label := range labels {
				if reflect.DeepEqual(pod.Labels, label) { //标签完全匹配
					ret = append(ret, pod)
				}
			}
		}
		return ret, nil
	}
	return nil, fmt.Errorf("pods not found ")
}

// GetNameSpaceList 获取namespace列表 （kubectl get ns -A）
func (s *runtimeStore) GetNameSpaceList() []string {
	var list []string
	s.cacheStore.Range(func(key, value interface{}) bool {
		if obj, ok := value.(*corev1.Namespace); ok {
			list = append(list, obj.Namespace)
		}
		return true
	})
	return list
}

// GetPodMessage 用来保存事件，只保存最新一条。 确保其唯一
func (s *runtimeStore) GetPodMessage(ns, kind, name string) string {
	key := eventPodMessagePrefix(ns, kind, name)
	if v, ok := s.cacheStore.Load(key); ok {
		return v.(*corev1.Event).Message
	}
	return ""
}

// GetIngress 返回指定的ingress
func (s *runtimeStore) GetIngress(namespcae, name string) *v1beta1.Ingress {
	if items, ok := s.cacheStore.Load(ingressPrefix(namespcae)); ok {
		for _, item := range items.([]*v1beta1.Ingress) {
			if item.Name == name {
				return item
			}
		}
	}
	return nil
}

// GetIngressList 获取ingress列表
func (s *runtimeStore) GetIngressList(namespace string) ([]*v1beta1.Ingress, error) {
	if list, ok := s.cacheStore.Load(ingressPrefix(namespace)); ok {
		newList := list.([]*v1beta1.Ingress)
		ret := make([]*v1beta1.Ingress, len(newList))
		for i, item := range newList {
			ret[i] = item
		}
		return ret, nil
	}
	return nil, fmt.Errorf("ingress not found ")
}
