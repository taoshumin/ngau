package informer

import (
	"k8s.io/client-go/listers/apps/v1"
	corev1 "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
)

type Informer struct {
	Namespace                cache.SharedIndexInformer
	Ingress                  cache.SharedIndexInformer
	Service                  cache.SharedIndexInformer
	Secret                   cache.SharedIndexInformer
	StatefulSet              cache.SharedIndexInformer
	Deployment               cache.SharedIndexInformer
	Pod                      cache.SharedIndexInformer
	ConfigMap                cache.SharedIndexInformer
	ReplicaSet               cache.SharedIndexInformer
	Endpoints                cache.SharedIndexInformer
	Nodes                    cache.SharedIndexInformer
	StorageClass             cache.SharedIndexInformer
	Claims                   cache.SharedIndexInformer
	Events                   cache.SharedIndexInformer
	HorizontalPodAutoscaling cache.SharedIndexInformer
}

type Lister struct {
	Namespace  corev1.NamespaceLister
	Deployment v1.DeploymentLister
	Pod        corev1.PodLister
	Events     corev1.EventLister
}

func (i *Informer) Start(stop <-chan struct{}) {
	go i.Namespace.Run(stop)
	//go i.Ingress.Run(stop)
	//go i.Service.Run(stop)
	//go i.Secret.Run(stop)
	//go i.StatefulSet.Run(stop)
	go i.Deployment.Run(stop)
	go i.Pod.Run(stop)
	//go i.ConfigMap.Run(stop)
	//go i.ReplicaSet.Run(stop)
	//go i.Endpoints.Run(stop)
	//go i.Nodes.Run(stop)
	//go i.StorageClass.Run(stop)
	//go i.Claims.Run(stop)
	//go i.Events.Run(stop)
	//go i.HorizontalPodAutoscaling.Run(stop)
}

func (i *Informer) Ready() bool {
	if i.Namespace.HasSynced() && i.Ingress.HasSynced() && i.Service.HasSynced() && i.Secret.HasSynced() &&
		i.StatefulSet.HasSynced() && i.Deployment.HasSynced() && i.Pod.HasSynced() &&
		i.ConfigMap.HasSynced() && i.Nodes.HasSynced() && i.Events.HasSynced() &&
		i.HorizontalPodAutoscaling.HasSynced() && i.StorageClass.HasSynced() && i.Claims.HasSynced() {
		return true
	}
	return false
}
