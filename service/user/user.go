package user

import (
	"context"
	"x6t.io/ngau/core"
)

type service struct {
}

// Find returns the authenticated user.
func (s *service) Find(ctx context.Context, access, refresh string) (*core.User, error) {
	return nil, nil
}

// FindLogin returns a user by username.
func (s *service) FindLogin(ctx context.Context, user *core.User, login string) (*core.User, error) {
	return nil, nil
}
