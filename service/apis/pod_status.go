/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apis

import (
	v1 "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

type PodStatusService struct {
	kubernetes.Interface
}

// IsReady （判断pod是否就绪）废弃版本
func (s *PodStatusService) IsReady(pod *core.Pod) bool {
	if pod.ObjectMeta.DeletionTimestamp != nil {
		return false
	}
	for _, gate := range pod.Spec.ReadinessGates {
		for _, condition := range pod.Status.Conditions {
			if condition.Type == gate.ConditionType && condition.Status != core.ConditionTrue {
				return false
			}
		}
	}
	return true
}

// PodIsReady 第一个版本的省级版
func (s *PodStatusService) PodIsReady(pod *core.Pod) bool {
	if pod.Status.Phase != core.PodRunning {
		return false
	}

	for _, condition := range pod.Status.Conditions {
		if condition.Status != core.ConditionTrue {
			return false
		}
	}

	for _, gate := range pod.Spec.ReadinessGates {
		for _, condition := range pod.Status.Conditions {
			if condition.Type == gate.ConditionType && condition.Status != core.ConditionTrue {
				return false
			}
		}
	}
	return true
}

// StatusMessage （根据Conditions返回Pod错误信息）
func (s *PodStatusService) StatusMessage(pod core.Pod) (message string) {
	for _, cond := range pod.Status.Conditions {
		if cond.Status != "True" {
			message += cond.Message
		}
	}
	return
}

/*
	查看Deployment详细信息
	kubectl describe rs nginx-deployment-7b45d69949 -n mylearn
	Selector:       app=nginx,pod-template-hash=7b45d69949
	Labels:         app=nginx
	pod-template-hash=7b45d69949
	Annotations:    deployment.kubernetes.io/desired-replicas: 2
	deployment.kubernetes.io/max-replicas: 3
	deployment.kubernetes.io/revision: 3
	deployment.kubernetes.io/revision-history: 1

	查看RS的详细信息
	kubectl get rs -n mylearn

	kubectl describe deployment nginx-deployment -n mylearn
	Annotations:            deployment.kubernetes.io/revision: 3
	NewReplicaSet:   nginx-deployment-7b45d69949 (2/2 replicas created)

	查看Pod的详细信息
	kubectl describe pod nginx-deployment-7b45d69949-f2l5p -n mylearn
	Labels:       app=nginx
	pod-template-hash=7b45d69949
*/
// IsCurrentLabel （判断当前的rs是否是最新的）
func (s *PodStatusService) IsCurrentLabel(deployment *v1.Deployment, rs v1.ReplicaSet) bool {
	if rs.ObjectMeta.Annotations["deployment.kubernetes.io/revision"] != deployment.ObjectMeta.Annotations["deployment.kubernetes.io/revision"] {
		return false
	}
	return s.IsRsFromDep(deployment, rs)
}

// IsRsFromDep （判断rs是否属于某个dep）
func (s *PodStatusService) IsRsFromDep(deployment *v1.Deployment, rs v1.ReplicaSet) bool {
	for _, reference := range rs.OwnerReferences {
		if reference.Kind == "Deployment" && reference.Name == deployment.Name {
			return true
		}
	}
	return false
}
