/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apis

import (
	"context"
	"fmt"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	meta1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"x6t.io/ngau/core"
	"x6t.io/ngau/models"
)

type DeploymentService struct {
	kubernetes.Interface
	pod     core.PodStore
	runtime core.RuntimeStore
}

func NewDeploymentService(kube kubernetes.Interface, pod core.PodStore, runtime core.RuntimeStore) *DeploymentService {
	return &DeploymentService{
		Interface: kube,
		pod:       pod,
		runtime:   runtime,
	}
}

// List (根据api获取Deployment列表,不推荐使用)
// Deprecated: Please use ListByWatch instead
func (s *DeploymentService) List(ctx context.Context, nameSpace string) ([]*models.Deployment, error) {
	opts := meta1.ListOptions{}
	list, err := s.AppsV1().
		Deployments(nameSpace).
		List(ctx, opts)
	if err != nil {
		return nil, err
	}
	items := make([]*models.Deployment, len(list.Items))
	for i, item := range list.Items {
		items[i] = &models.Deployment{
			Name:       item.Name,
			NameSpace:  item.Namespace,
			Images:     s.Images(item),
			Message:    s.DeploymentCondition(&item),
			IsComplete: s.IsDeploymentComplete(&item),
			Replicas: [3]int32{
				item.Status.Replicas,
				item.Status.AvailableReplicas,
				item.Status.UnavailableReplicas,
			},
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
		}
	}
	return items, nil
}

// ListByWatch (根据Informer实时获取Deployment列表，推荐使用)
func (s *DeploymentService) ListByWatch(ctx context.Context, nameSpace string) ([]*models.Deployment, error) {
	list, err := s.runtime.GetDeploymentListByNameSpace(nameSpace)
	if err != nil {
		return nil, err
	}
	items := make([]*models.Deployment, len(list))
	for i, item := range list {
		items[i] = &models.Deployment{
			Name:       item.Name,
			NameSpace:  item.Namespace,
			Images:     s.Images(*item),
			Message:    s.DeploymentCondition(item),
			IsComplete: s.IsDeploymentComplete(item),
			Replicas: [3]int32{
				item.Status.Replicas,
				item.Status.AvailableReplicas,
				item.Status.UnavailableReplicas,
			},
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
		}
	}
	return items, nil
}

// Create （创建简单的Deployment）
func (s *DeploymentService) Create(ctx context.Context, req core.Deployment) error {
	_, err := s.AppsV1().
		Deployments(req.Namespace).
		Create(ctx, &v1.Deployment{
			ObjectMeta: meta1.ObjectMeta{
				Namespace: req.Namespace,
				Name:      req.Name,
			},
			Spec: v1.DeploymentSpec{
				Selector: &meta1.LabelSelector{
					MatchLabels: s.genLabels(req),
				},
				Template: corev1.PodTemplateSpec{
					ObjectMeta: meta1.ObjectMeta{
						Labels: s.genLabels(req),
					},
					Spec: corev1.PodSpec{
						Containers: s.genContainers(req),
					},
				},
			},
		}, meta1.CreateOptions{})
	return err
}

// Detail (根据Deployment的名称获取Deployment详细信息)
func (s *DeploymentService) Detail(ctx context.Context, nameSpace, name string) (*models.Deployment, error) {
	opts := meta1.GetOptions{}
	item, err := s.AppsV1().
		Deployments(nameSpace).
		Get(ctx, name, opts)
	if err != nil {
		return nil, err
	}
	return &models.Deployment{
		Name:       item.Name,
		NameSpace:  item.Namespace,
		Images:     s.Images(*item),
		Message:    s.DeploymentCondition(item),
		Pods:       s.pod.ListsByDeployment(ctx, item, nameSpace),
		CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
	}, nil
}

// Scale (动态扩容Deployment，如果desc是true,则减减，反之则加加)
func (s *DeploymentService) Scale(ctx context.Context, nameSpace, deployment string, desc bool) error {
	opts := meta1.GetOptions{}
	scale, err := s.AppsV1().
		Deployments(nameSpace).
		GetScale(ctx, deployment, opts)
	if err != nil {
		return err
	}
	if desc {
		scale.Spec.Replicas--
	} else {
		scale.Spec.Replicas++
	}
	_, err = s.AppsV1().
		Deployments(nameSpace).
		UpdateScale(ctx, deployment, scale, meta1.UpdateOptions{})
	return err
}

// genLabels （创建Deployment的时候简单设置标签Label）
func (s *DeploymentService) genLabels(req core.Deployment) map[string]string {
	return map[string]string{
		"app": req.Name,
	}
}

// genContainers (创建Deployment的时候简单设置容器的名称)
func (s *DeploymentService) genContainers(req core.Deployment) []corev1.Container {
	// 获取Deployment的Port列表
	ports := make([]corev1.ContainerPort, len(req.Ports))
	for i, port := range req.Ports {
		ports[i].Name = fmt.Sprintf("%s%d", req.Name, port)
		ports[i].Protocol = "TCP"
		ports[i].ContainerPort = req.Ports[i]
	}
	// 创建Deployment的Port列表
	ret := make([]corev1.Container, 1)
	ret[0] = corev1.Container{
		Name:  req.Name,
		Image: req.Images,
		Ports: ports,
	}
	return ret
}

// Images （根据Deployment获取容器镜像名称，一个Pod可能包含多个镜像名称）
func (s *DeploymentService) Images(item v1.Deployment) string {
	return s.pod.Images(item.Spec.Template.Spec.Containers)
}

// IsDeploymentComplete （判断Deployment的是否完成）
func (s *DeploymentService) IsDeploymentComplete(dep *v1.Deployment) bool {
	// Deployment副本数 = Deployment可用副本数
	return dep.Status.Replicas == dep.Status.AvailableReplicas
}

// DeploymentCondition (查看pod就绪状态,如果还未就绪，返回错误信息)
func (s *DeploymentService) DeploymentCondition(dep *v1.Deployment) string {
	for _, item := range dep.Status.Conditions {
		if string(item.Type) == "Available" && string(item.Status) != "True" {
			return item.Message
		}
	}
	return ""
}
