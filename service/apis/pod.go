/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apis

import (
	"context"
	"fmt"
	"k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	meta1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"strings"
	corevv "x6t.io/ngau/core"
	"x6t.io/ngau/models"
)

type PodService struct {
	kubernetes.Interface
	runtime corevv.RuntimeStore
	status  corevv.PodStatusStore
	events  corevv.PodEventStore
}

func NewPodService(kube kubernetes.Interface, status corevv.PodStatusStore, events corevv.PodEventStore) *PodService {
	return &PodService{
		Interface: kube,
		status:    status,
		events:    events,
	}
}

// ListByNameSpace 省级版本 建议使用这个
func (s *PodService) ListByNameSpace(ctx context.Context, namespace string) []models.Pod {
	list, err := s.runtime.GetPodListByNameSpace(namespace)
	if err != nil {
		return nil
	}
	pods := make([]models.Pod, len(list))
	for i, item := range list {
		pods[i] = models.Pod{
			Name:     item.Name,
			Images:   s.Images(item.Spec.Containers),
			NodeName: item.Spec.NodeName,
			IP:       []string{item.Status.HostIP, item.Status.PodIP},
			// 获取状态基于两个点,与第一个版本不同
			// Pod的phase为running
			// Pod的PodConditions所有状态军为true
			Phase:   string(item.Status.Phase),
			IsReady: s.status.PodIsReady(item),
			//Message:    s.status.StatusMessage(item),
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
		}
	}
	return pods
}

// ListsByDeployment (根据Deployment获取当前Deployment的中的Pod列表，不准确) TODO 废弃
func (s *PodService) ListsByDeployment(ctx context.Context, deployment *v1.Deployment, nameSpace string) []models.Pod {
	opts := meta1.ListOptions{
		LabelSelector: s.Labels(deployment.Spec.Selector.MatchLabels),
	}
	list, err := s.CoreV1().
		Pods(nameSpace).
		List(ctx, opts)
	if err != nil {
		return nil
	}

	pods := make([]models.Pod, len(list.Items))
	for i, item := range list.Items {
		pods[i] = models.Pod{
			Name:       item.Name,
			Images:     s.Images(item.Spec.Containers),
			NodeName:   item.Spec.NodeName,
			Phase:      string(item.Status.Phase),
			Message:    s.status.StatusMessage(item),
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
		}
	}
	return pods
}

// ListsByDeploymentUseRs  (根据Deployment获取当前最新的RS标签，根据标签获取当前Pod的列表)
func (s *PodService) ListsByDeploymentUseRs(ctx context.Context, deployment *v1.Deployment, nameSpace string) []models.Pod {
	opts := meta1.ListOptions{
		LabelSelector: s.RsLabelByDeployment(ctx, deployment),
	}
	list, err := s.CoreV1().
		Pods(nameSpace).
		List(ctx, opts)
	if err != nil {
		return nil
	}

	pods := make([]models.Pod, len(list.Items))
	for i, item := range list.Items {
		pods[i] = models.Pod{
			Name:       item.Name,
			NameSpace:  item.Namespace,
			Images:     s.Images(item.Spec.Containers),
			NodeName:   item.Spec.NodeName,
			Phase:      string(item.Status.Phase),
			Message:    s.status.StatusMessage(item), // change event
			IsReady:    s.status.IsReady(&item),
			IP:         []string{item.Status.HostIP, item.Status.PodIP},
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:03:04"),
		}
	}
	return pods
}

// RsLabelByDeployment （根据Deployment获取当前rs的标签）
func (s *PodService) RsLabelByDeployment(ctx context.Context, deployment *v1.Deployment) string {
	selector, err := meta1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		return ""
	}
	listOpts := meta1.ListOptions{
		LabelSelector: selector.String(),
	}

	rs, err := s.AppsV1().
		ReplicaSets(deployment.Namespace).
		List(ctx, listOpts)
	if err != nil {
		return ""
	}

	for _, item := range rs.Items {
		if s.status.IsCurrentLabel(deployment, item) {
			sel, err := meta1.LabelSelectorAsSelector(item.Spec.Selector)
			if err != nil {
				return ""
			}
			return sel.String()
		}
	}
	return ""
}

// RsLabelByDeploymentListWatch (根据deployment获取所有的rs标签，给list Watch使用)
func (s *PodService) RsLabelByDeploymentListWatch(ctx context.Context, deployment *v1.Deployment, rss []*v1.ReplicaSet) ([]map[string]string, error) {
	ret := make([]map[string]string, 0)
	for _, item := range rss {
		if s.status.IsRsFromDep(deployment, *item) {
			sel, err := meta1.LabelSelectorAsMap(item.Spec.Selector)
			if err != nil {
				return nil, err
			}
			ret = append(ret, sel)
		}
	}
	return ret, nil
}

// Delete (删除Pod)
func (s *PodService) Delete(ctx context.Context, nameSpace, podName string) error {
	return s.CoreV1().
		Pods(nameSpace).
		Delete(ctx, podName, meta1.DeleteOptions{})
}

// PodJSON (查看Pod的JSON格式)
func (s *PodService) PodJSON(ctx context.Context, nameSpace string, podName string) (*core.Pod, error) {
	pod, err := s.CoreV1().
		Pods(nameSpace).
		Get(ctx, podName, meta1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return pod, nil
}

// Images （获取当前容器主镜像名称和其他镜像名称）
func (s *PodService) Images(containers []core.Container) string {
	images := containers[0].Image
	if length := len(containers); length > 1 {
		images += fmt.Sprintf(" + 其他%d个镜像", length-1)
	}
	return images
}

// Labels (构建Label标签)
func (s *PodService) Labels(label map[string]string) string {
	var labels []string
	for k, v := range label {
		labels = append(labels, fmt.Sprintf("%s=%s", k, v))
	}
	// example aa=xxx,bb=xxx
	return strings.Join(labels, ",")
}
