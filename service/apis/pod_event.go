/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apis

import (
	"context"
	"fmt"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type PodEventsService struct {
	kubernetes.Interface
}

// Events (InvolvedObject获取pod事件错误详细信息,此事件信息仅保留一小时)
func (s *PodEventsService) Events(ctx context.Context, nameSpace string) {
	list, err := s.CoreV1().
		Events(nameSpace).
		List(ctx, v1.ListOptions{})
	if err != nil {
		return
	}

	for _, item := range list.Items {
		fmt.Println(item.InvolvedObject)
		fmt.Println(item.Message)
		fmt.Println(item.Kind)
	}
}
