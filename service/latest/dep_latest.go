/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package latest

import (
	v1 "k8s.io/api/apps/v1"
	"k8s.io/client-go/kubernetes"
	corevv "x6t.io/ngau/core"
	"x6t.io/ngau/models"
)

type DeploymentLatestService struct {
	kubernetes.Interface
	runtime corevv.RuntimeStore
	common  *CommonService
}

func (*DeploymentLatestService) getDeploymentCondition(dep *v1.Deployment) string {
	for _, item := range dep.Status.Conditions {
		if string(item.Type) == "Available" && string(item.Status) != "True" {
			return item.Message
		}
	}
	return ""
}

func (*DeploymentLatestService) getDeploymentIsComplete(dep *v1.Deployment) bool {
	return dep.Status.Replicas == dep.Status.AvailableReplicas
}

func (s *DeploymentLatestService) ListAll(namespace string) (ret []*models.Deployment) {
	depList, err := s.runtime.GetDeploymentListByNameSpace(namespace)
	if err != nil {
		return
	}
	for _, item := range depList {
		ret = append(ret, &models.Deployment{Name: item.Name,
			NameSpace:  item.Namespace,
			Replicas:   [3]int32{item.Status.Replicas, item.Status.AvailableReplicas, item.Status.UnavailableReplicas},
			Images:     s.common.GetImages(*item),
			IsComplete: s.getDeploymentIsComplete(item),
			Message:    s.getDeploymentCondition(item),
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:04:05"),
		})
	}
	return
}
