/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package latest

import (
	"k8s.io/client-go/kubernetes"
	corevv "x6t.io/ngau/core"
	"x6t.io/ngau/models"
)

type PodLatestService struct {
	kubernetes.Interface
	runtime corevv.RuntimeStore
	common  *CommonService
}

func (s *PodLatestService) ListByNs(ns string) []*models.Pod {
	podList, err := s.runtime.GetPodListByNameSpace(ns)
	if err != nil {
		return nil
	}
	ret := make([]*models.Pod, 0)
	for _, pod := range podList {
		ret = append(ret, &models.Pod{
			Name:       pod.Name,
			NameSpace:  pod.Namespace,
			Images:     s.common.GetImagesByPod(pod.Spec.Containers),
			NodeName:   pod.Spec.NodeName,
			Phase:      string(pod.Status.Phase), // 阶段
			IsReady:    s.common.PodIsReady(pod), // 就绪
			Message:    s.runtime.GetPodMessage(pod.Namespace, "Pod", pod.Name),
			CreateTime: pod.CreationTimestamp.Format("2006-01-02 15:04:05"),
			IP:         []string{pod.Status.PodIP, pod.Status.HostIP},
		})
	}
	return ret
}
