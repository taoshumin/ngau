/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package latest

import (
	"fmt"
	v1 "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
	"x6t.io/ngau/models"
)

type CommonService struct{}

func (*CommonService) PodIsReady(pod *core.Pod) bool {
	if pod.Status.Phase != core.PodRunning {
		return false
	}
	for _, condition := range pod.Status.Conditions {
		if condition.Status != core.ConditionTrue {
			return false
		}
	}

	for _, gate := range pod.Spec.ReadinessGates {
		for _, condition := range pod.Status.Conditions {
			if condition.Type == gate.ConditionType && condition.Status != core.ConditionTrue {
				return false
			}
		}
	}
	return true
}

func (*CommonService) GetImagesByPod(containers []corev1.Container) string {
	images := containers[0].Image
	if imgLen := len(containers); imgLen > 1 {
		images += fmt.Sprintf("+其他%d个镜像", imgLen-1)
	}
	return images
}

func (s *CommonService) GetImages(dep v1.Deployment) string {
	return s.GetImagesByPod(dep.Spec.Template.Spec.Containers)
}

func (s *CommonService) ParseCert(data []byte) *models.Cert {
	return nil
}
