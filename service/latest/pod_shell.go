/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package latest

import (
	"context"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
	"os"
)

type PodShellService struct {
	kubernetes.Interface
}

// PodByShell 调用远程Pod终端,简称Webshell
// 可参考： http://maoqide.live/post/cloud/kubernetes-webshell/
func (s *PodShellService) PodByShell(ctx context.Context, cfg *rest.Config) {
	// 创建请求对象, SPDY
	req := s.CoreV1().RESTClient().Post().
		Resource("pods").
		Namespace("default").
		Name("nginx-").
		SubResource("exec").
		VersionedParams(
			&v1.PodExecOptions{
				Stdin:     true,
				Stdout:    true,
				Stderr:    true,
				Container: "nginx",
				Command:   []string{"sh", "-c", "ls"},
			},
			scheme.ParameterCodec)

	// 远程调用
	executor, err := remotecommand.NewSPDYExecutor(cfg, "POST", req.URL())
	if err != nil {
		return
	}
	err = executor.Stream(remotecommand.StreamOptions{
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
		Tty:    true,
	})
	if err != nil {
		return
	}
}
