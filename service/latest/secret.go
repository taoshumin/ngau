/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package latest

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"x6t.io/ngau/models"
)

type SecretService struct {
	kubernetes.Interface
	common CommonService
}

func (s *SecretService) Get(ctx context.Context, ns, name string) (*models.Secret, error) {
	secret, err := s.CoreV1().Secrets(ns).Get(ctx, name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return &models.Secret{
		Name:       secret.Name,
		NameSpace:  secret.Namespace,
		Type:       string(secret.Type),
		CreateTime: secret.CreationTimestamp.Format("2006-01-02 15:04:05"),
		Data:       secret.Data,
	}, nil
}

func (s *SecretService) List(ctx context.Context, ns string) []*models.Secret {
	l, err := s.CoreV1().Secrets(ns).List(ctx, v1.ListOptions{})
	if err != nil {
		return nil
	}
	list := l.Items
	ret := make([]*models.Secret, len(list))
	for i, item := range list {
		ret[i] = &models.Secret{
			Name:       item.Name,
			NameSpace:  item.Namespace,
			Type:       models.SecretType[string(item.Type)],
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:04:05"),
			Data:       item.Data,
		}
	}
	return ret
}

// Create 自定义类型 默认: Opaque
// kubectl create secret tls  tls-secret --cert=path/to/tls.cert --key=path/to/tls.key
func (s *SecretService) Create(ctx context.Context, sm models.Secret) error {
	_, err := s.CoreV1().Secrets(sm.NameSpace).Create(ctx, &corev1.Secret{
		ObjectMeta: v1.ObjectMeta{
			Name:      sm.Name,
			Namespace: sm.NameSpace,
		},
		Type:       corev1.SecretType(sm.Type), // Opaque
		StringData: sm.PostData,
	}, v1.CreateOptions{})
	return err
}

// ParseTlsSecret 解析 （比如类型是tls的secret）
func (s *SecretService) ParseTlsSecret(t string, data map[string][]byte) interface{} {
	if t == "kubernetes.io/tls" {
		if crt, ok := data["tls.crt"]; ok {
			certModel := s.common.ParseCert(crt)
			if certModel != nil {
				return certModel
			}
		}
	}
	return nil
}
