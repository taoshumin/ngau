/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Example YAML
/*
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
  namespace: default
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
    - host: test.xiadat.com
      http:
        paths:
          - path: /
            backend:
              serviceName: nginx
              servicePort: 80
*/

package latest

import (
	"context"
	"github.com/spf13/cast"
	v1beta1 "k8s.io/api/networking/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"strings"
	corevv "x6t.io/ngau/core"
	"x6t.io/ngau/models"
)

const (
	OPTION_CROS = iota
	OPTION_LIMIT
)

const OPTOINS_CROS_TAG = "nginx.ingress.kubernetes.io/enable-cors"

type IngressLatestService struct {
	kubernetes.Interface
	runtime corevv.RuntimeStore
}

func (s *IngressLatestService) ListByNameSpace(ctx context.Context, namespace string) ([]models.Ingress, error) {
	list, err := s.runtime.GetIngressList(namespace)
	if err != nil {
		return nil, err
	}

	items := make([]models.Ingress, len(list))
	for i, item := range list {
		items[i] = models.Ingress{
			Name:       item.Name,
			Namespace:  item.Namespace,
			Host:       item.Spec.Rules[0].Host,
			Options:    models.IngressOptions{IsCrops: s.getIngressOptions(OPTION_CROS, item)},
			CreateTime: item.CreationTimestamp.Format("2006-01-02 15:04:05"),
		}
	}
	return items, nil
}

func (s *IngressLatestService) CreateIngress(ctx context.Context, model *models.IngressRequest) error {
	classname := "nginx"
	ingressRules := []v1beta1.IngressRule{}
	for _, r := range model.Rules {
		httpRule := &v1beta1.HTTPIngressRuleValue{}
		rulePath := make([]v1beta1.HTTPIngressPath, 0)
		for _, path := range r.Paths {
			port := cast.ToInt(path.Port)
			rulePath = append(rulePath, v1beta1.HTTPIngressPath{
				Path: path.Path,
				Backend: v1beta1.IngressBackend{
					ServiceName: path.SvcName,
					ServicePort: intstr.FromInt(port),
				},
			})
		}
		httpRule.Paths = rulePath
		rule := v1beta1.IngressRule{
			Host: r.Host,
			IngressRuleValue: v1beta1.IngressRuleValue{
				HTTP: httpRule,
			},
		}

		ingressRules = append(ingressRules, rule)
	}

	ingress := &v1beta1.Ingress{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Ingress",
			APIVersion: "networking.k8s.io/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        model.Name,
			Namespace:   model.Namespace,
			Annotations: s.parseAnnotations(model.Annotations),
		},
		Spec: v1beta1.IngressSpec{
			IngressClassName: &classname,
			Rules:            ingressRules,
		},
		Status: v1beta1.IngressStatus{},
	}

	_, err := s.NetworkingV1beta1().Ingresses(model.Namespace).
		Create(ctx, ingress, metav1.CreateOptions{})

	return err
}

// parseAnnotations 解析标签
func (s *IngressLatestService) parseAnnotations(annos string) map[string]string {
	replace := []string{"\t", " ", "\n", "\r\n"}
	for _, r := range replace {
		annos = strings.ReplaceAll(annos, r, "")
	}
	ret := make(map[string]string)
	list := strings.Split(annos, ";")
	for _, item := range list {
		annos := strings.Split(item, ":")
		if len(annos) == 2 {
			ret[annos[0]] = annos[1]
		}
	}
	return ret
}

func (s *IngressLatestService) Dlete(ctx context.Context, namespace, name string) error {
	return s.NetworkingV1beta1().
		Ingresses(namespace).
		Delete(ctx, name, metav1.DeleteOptions{})
}

// 跨域设置
func (s *IngressLatestService) getIngressOptions(t int, item *v1beta1.Ingress) bool {
	switch t {
	case OPTION_CROS:
		if _, ok := item.Annotations[OPTOINS_CROS_TAG]; ok {
			return true
		}
	}
	return false
}
