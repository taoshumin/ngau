/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package core

import (
	"context"
	v1 "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/networking/v1beta1"
	"x6t.io/ngau/models"
)

type (
	Deployment struct {
		Name      string  `json:"name"`
		Namespace string  `json:"namespace"`
		Images    string  `json:"images"`
		Ports     []int32 `json:"ports"`
	}

	PodStore interface {
		ListByNameSpace(ctx context.Context, namespace string) []models.Pod
		ListsByDeployment(ctx context.Context, deployment *v1.Deployment, nameSpace string) []models.Pod
		ListsByDeploymentUseRs(ctx context.Context, deployment *v1.Deployment, nameSpace string) []models.Pod
		RsLabelByDeployment(ctx context.Context, deployment *v1.Deployment) string
		Delete(ctx context.Context, nameSpace, podName string) error
		Images(containers []core.Container) string
		PodJSON(ctx context.Context, nameSpace string, podName string) (*core.Pod, error)
	}

	PodStatusStore interface {
		// IsReady Deprecated
		IsReady(pod *core.Pod) bool
		PodIsReady(pod *core.Pod) bool
		StatusMessage(pod core.Pod) (message string)
		IsCurrentLabel(deployment *v1.Deployment, rs v1.ReplicaSet) bool
		IsRsFromDep(deployment *v1.Deployment, rs v1.ReplicaSet) bool
	}

	PodEventStore interface {
		Events(ctx context.Context, nameSpace string)
	}

	DeploymentStore interface {
		Create(ctx context.Context, req Deployment) error
		List(ctx context.Context, nameSpace string) ([]*models.Deployment, error)
		ListByWatch(ctx context.Context, nameSpace string) ([]*models.Deployment, error)
		Detail(ctx context.Context, nameSpace, name string) (*models.Deployment, error)
		Scale(ctx context.Context, nameSpace, deployment string, desc bool) error
	}

	NameSpaceStore interface {
		List(ctx context.Context) []string
	}

	RuntimeStore interface {
		GetDeploymentListByNameSpace(nameSpace string) ([]*v1.Deployment, error)
		GetDeployment(ns, depname string) (*v1.Deployment, error)
		GetPodListByNameSpace(nameSpace string) ([]*corev1.Pod, error)
		GetPod(namespace, podname string) *corev1.Pod
		GetPodListByLabel(namespace string, labels []map[string]string) ([]*corev1.Pod, error)
		GetNameSpaceList() []string
		GetPodMessage(ns, kind, name string) string
		GetIngress(namespcae, name string) *v1beta1.Ingress
		GetIngressList(namespace string) ([]*v1beta1.Ingress, error)
	}
)
