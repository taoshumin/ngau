/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package models

type Deployment struct {
	Name       string   `json:"name,omitempty"`        // Deployment 名称
	NameSpace  string   `json:"name_space,omitempty"`  // Deployment 命名空间
	Images     string   `json:"images,omitempty"`      // Pod 镜像名称
	CreateTime string   `json:"create_time,omitempty"` // Deployment 创建时间
	IsComplete bool     `json:"is_complete,omitempty"` // Deployment 是否完成
	Message    string   `json:"message,omitempty"`     // Deployment 显示错误信息
	Pods       []Pod    `json:"pods,omitempty"`
	Replicas   [3]int32 `json:"replicas,omitempty"` // 分别是总副本数，可用副本数 ，不可用副本数
}
