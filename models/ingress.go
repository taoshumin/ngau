/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package models

type Ingress struct {
	Name       string
	Namespace  string
	Host       string
	Options    IngressOptions
	CreateTime string
}

type IngressOptions struct {
	IsCrops bool
}

// IngressRequest 请求参数
type IngressRequest struct {
	Name        string          `json:"name"`
	Namespace   string          `json:"namespace"`
	Rules       []*IngressRules `json:"rules"`
	Annotations string          `json:"annotations"` //标签
}

type IngressRules struct {
	Host  string         `json:"host"`
	Paths []*IngressPath `json:"paths"`
}

type IngressPath struct {
	Path    string `json:"path"`
	SvcName string `json:"svc_name"`
	Port    string `json:"port"`
}
