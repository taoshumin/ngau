/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package models

type Pod struct {
	Name       string   `json:"name,omitempty"`        // Pod 名称
	NameSpace  string   `json:"name_space,omitempty"`  // Pod 命名空间
	Images     string   `json:"images,omitempty"`      // Pod 镜像名
	NodeName   string   `json:"node_name,omitempty"`   // Node 节点名称
	Phase      string   `json:"phase,omitempty"`       // Pod 所处的阶段
	Message    string   `json:"message,omitempty"`     // Pod 错误信息等
	IsReady    bool     `json:"is_ready,omitempty"`    // 判断Pod是否就绪
	CreateTime string   `json:"create_time,omitempty"` // Pod 创建时间
	IP         []string `json:"ip,omitempty"`          // 第一个Pod的IP,第二个Node的IP
}
