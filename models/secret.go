/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package models

var SecretType map[string]string

type Secret struct {
	Name       string            `json:"name,omitempty"`
	NameSpace  string            `json:"name_space,omitempty"`
	Type       string            `json:"type,omitempty"`
	CreateTime string            `json:"create_time,omitempty"`
	Data       map[string][]byte `json:"data,omitempty"`
	PostData   map[string]string
}

type Cert struct {
	CN        string // 域名
	Algorithm string // 算法
	Issuer    string // 签发者
	BeginTime string // 生效时间
	EndTime   string // 到期时间
}

func init() {
	SecretType = map[string]string{
		"Opaque":                              "自定义类型",
		"kubernetes.io/service-account-token": "服务账号令牌",
		"kubernetes.io/dockercfg":             "docker配置",
		"kubernetes.io/dockerconfigjson":      "docker配置JSON",
		"kubernetes.io/basic-auth":            "Basic认证凭据",
		"kubernetes.io/ssh-auth":              "SSH 身份认证的凭据",
		"kubernetes.io/tls":                   "TSL凭据",
		"bootstrap.kubernetes.io/token":       "启动引导令牌数据",
	}
}
