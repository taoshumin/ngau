/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package client

import (
	"k8s.io/client-go/kubernetes"
	"reflect"
	"testing"
)

func TestNewProxyClientSet(t *testing.T) {
	type args struct {
		address string
	}
	tests := []struct {
		name    string
		args    args
		want    kubernetes.Interface
		wantErr bool
	}{
		{
			name:    "connect",
			args:    args{address: "http://106.12.78.172:8080"},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewProxyClientSet(tt.args.address)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewProxyClientSet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProxyClientSet() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewClientSet(t *testing.T) {
	type args struct {
		kubecfg string
	}
	tests := []struct {
		name    string
		args    args
		want    kubernetes.Interface
		wantErr bool
	}{
		{
			name: "config",
			args: args{
				kubecfg: "config",
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewClientSet(tt.args.kubecfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewClientSet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewClientSet() got = %v, want %v", got, tt.want)
			}
		})
	}
}
