# 一、Ngau

### 扩展知识

-  获取ApiEndPoint

```api
$ cat ~/.kube/config
$ kubectl get endpoints
```

- 获取Token

```api
$ kubectl get sa
$ kubectl get secret default-token-6chjz
$ kubectl get secret default-token-6chjz -o json
$ kubectl get secret default-token-6chjz -o json | jq -Mr '.data.token'
$ kubectl get secret default-token-6chjz -o json | jq -Mr '.data.token'| base64 -d
```

- 访问测试

```api
curl https://192.168.0.5:6443/version --insecure
```

备注：[K8S接口调用文档](https://v1-18.docs.kubernetes.io/docs/reference/generated/kubernetes-api/v1.18/#deployment-v1-apps)

### 1.相关基础知识

- 架构图

![Screenshot from 2021-09-23 22-52-42](docs/assets/Screenshot from 2021-09-23 22-52-42.png)

- informer流程介绍

```api
1.初始化时，调用List API获取全量list，缓存起来
2.调用Watch API去Watch资源，发生变更后会通过一定的机制维护缓存

解决：不需要每次请求都去请求ApiServer
```
- ReplicaSet 和Deployment介绍 

```
pod-template-hash介绍：

当Deployment创建或者接管ReplicaSet时，会自动添加pod-template-hash标签。通过将ReplicaSet的PodTemplate进行哈希散列，使用生成的哈希值作为label的值，并添加到ReplicaSet selector和ReplicaSet管理中的pod上。
```

### 2. Pod相关状态

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-09-24 23-11-57.png)

### 3. Pod状态细分 （具体原因）

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-09-24 23-20-54.png)

### 4. 获取Pod事件错误详细信息

`1小时后会被删除，防止磁盘被沾满`

```api
$ kubectl get events
LAST SEEN   TYPE     REASON    OBJECT        MESSAGE
38m         Normal   Pulled    pod/mynginx   Container image "alpine:3.12" already present on machine
38m         Normal   Created   pod/mynginx   Created container alpine
38m         Normal   Started   pod/mynginx   Started container alpine
```
备注： （InvolvedObject）
TYPE正常则是Normal,不正常则是其他比如Warning,Failed 

### 5. 判断Pod就绪状态

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-09-25 18-17-59.png)

### 6. Pod状况
![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-09-25 18-20-08.png)


### 6. Deployment 就绪状态
![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-09-25 21-00-34.png)


# 二、Client-go

### 参考文献

- http://kubernetes.kansea.com/docs/user-guide/kubectl/kubectl_apply/ (APIs 和 CLIs 的设计文档，概念定义以及引用)
- https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/ (YAML 定义文档)
- https://github.com/kubernetes/client-go  (k8s的client)
- https://www.cnblogs.com/xzkzzz/default.html?page=5 （k8s相关知识）

### 设置开放代理

```shell script
[root@jtthink1 ~]# kubectl proxy --address='0.0.0.0' --accept-hosts='^*$' --port=8080
Starting to serve on [::]:8080
```

### 安装（Install）

```api
$ go get k8s.io/client-go@latest
$ go get k8s.io/client-go@v.0.18.6
```

# 三、Ingress
- https://kubernetes.github.io/ingress-nginx/
- https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/ (跨域设置)
- https://www.jtthink.com/course/156 (import)

## helm3 

```shell script
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm fetch ingress-nginx/ingress-nginx
然后解压，进行各种修改
```

## 安装 nginx-ingress

`一定要创建一个 单独的namespace`：

```shell script
kubectl create ns nginx-ingress
helm install my-nginx ingress-nginx -n nginx-ingress
```

## 创建ingress的YAML

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
  namespace: default
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
    - host: test.xiadat.com
      http:
        paths:
          - path: /
            backend:
              serviceName: nginx
              servicePort: 80
```

# Secret 

- Secret类型
- https://kubernetes.io/zh/docs/concepts/configuration/secret/

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-10-24 20-22-34.png)

- 创建

```api
kubectl create secret generic empty-secret
```

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-11-07 19-52-38.png)

私钥，证书：

```api
kubectl create secret tls  tls-secret --cert=path/to/tls.cert --key=path/to/tls.key
```

# ConfigMap

![Screenshot from 2021-09-24 23-11-57.png](docs/assets/Screenshot from 2021-11-07 20-17-11.png)


# 四、Pod远程交互

- http://maoqide.live/post/cloud/kubernetes-webshell/
- https://github.com/xtermjs/xterm.js
- https://github.com/yudai/gotty
- https://github.com/maoqide/kubeutil (具体实现webshell参考)

注释： 不能使用proxy代理进行远程交互，必须设置配置文件 (vi ~/.kube/config)

- 修改地址: （server: https://106.12.78.172:6443）

```
   certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeE1ETXlPVEF6TXpNd05Gb1hEVE14TURNeU56QXpNek13TkZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTkgyCktCY21IUFhDNDEwN0RMLzNPWG0zMUovTlRZVFB3M3k1cUhBVnB3bDdibmlRT1dPVzZnRk9hUXBxemVEd3I1aW4KNlJnUmp3SDVxOTNqa0pUeGVzdXNMSXBqOTltdlRHaHNOdUY3RmI5Q0NPSTRvcjZEZW9abytEK0dGM1pnZjIzbApmQkV6UUF5Vit2Mk82bnBxN1M4K3FxbXZsYkM0a2RYUm1aWS8ydWJtODQzcStsNEFmZWhIYWFkblpIL3UyR0dSCmpMNFF2SnVTc01wNXdmSDJ6eCtMVUt1eU5XQ1pvQnF5WFVGa0JwV3VoSSt4WUFENiswcE9SQUJjRlgwM3ZLaWIKcFJjVGR3SWlOZURBejhVVWhmVXl4SjNYMXBHamJKRG5hWmtlVGRHL0xJekpMYnNwT3VmQ211YU84VXhuZlBKWQoxTXFLN3l0SWxXbThMNktqRWFrQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFFVjJ6ZEJVbEJFYUhlSzU0LzNXd054LzhYRncKV1JuOHRVVkRHeFhHT0EvRWNnVHR5WGNjUEZmbUlnSktSdEVyMnNEdCtiRG9DUXgwK0NsNTZTTmV1eXBYSUh1eApVK1NHZWw3RzNoU2xIMFh4dzZ5L082NnV6Ym9Cb1BXWVpVZ0tCSzQrQm11ZU1NUzhvYXZPRkc4ZUYrVXhDMnBZCnZlOFNreExqTmltSWxteCs1VGNyV05CS1A5RW5XS0Z2N2srcFZwRm41WEw3MWJMbnAvb0RiOGhPWGkvQmUvbGIKaHI0K3JXWkNBTWp5dDB3N3czaXFIY3NrTUIxM2x3TjBjS1RVNGthbGIyQWQ4NEUxZG5ndjZEU0UxQk5yZGFySgpBNUF6SGpQWnlQc2twKzJESUJON2p5ZHZlUmw2TzBzbFZ4bXUvNzNzNE5qR3NHZWNUdStKbEorSFIrcz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
    server: https://106.12.78.172:6443
```

- json inline

```
这个属性用作嵌套结构体内，消除嵌套结构体的层级关系，将其转为一个层级.
```

# 五 学习进度

https://www.jtthink.com/course/play/3372