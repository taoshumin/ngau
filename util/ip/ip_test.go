/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package ip

import "testing"

func TestCheckIP(t *testing.T) {
	t.Logf("ip %s %t", "1829.123", CheckIP("1829.123"))
	t.Logf("ip %s %t", "1829.123.1", CheckIP("1829.123.1"))
	t.Logf("ip %s %t", "1829.123.2.1", CheckIP("1829.123.2.1"))
	t.Logf("ip %s %t", "y.123", CheckIP("y.123"))
	t.Logf("ip %s %t", "0.0.0.0", CheckIP("0.0.0.0"))
	t.Logf("ip %s %t", "127.0.0.1", CheckIP("127.0.0.1"))
	t.Logf("ip %s %t", "localhost", CheckIP("localhost"))
	t.Logf("ip %s %t", "192.168.0.1", CheckIP("192.168.0.1"))
}
