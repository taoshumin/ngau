/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package ip

import (
	"context"
	"net"
	"sync"
	"time"
)

type EventType int

const (
	ADD EventType = iota
	DEL
	UPDATE
)

type Event struct {
	Type EventType
	IP   net.IP
}

type IpPoolService struct {
	ctx    context.Context
	cancel context.CancelFunc

	lock sync.RWMutex
	once sync.Once

	EventChannel chan Event
	readyChannel chan struct{}

	ignoreName []string
	hostIPs    map[string]net.IP
}

func NewIpPool(ignoreName []string) *IpPoolService {
	ctx, cancel := context.WithCancel(context.Background())
	return &IpPoolService{
		ignoreName:   ignoreName,
		ctx:          ctx,
		cancel:       cancel,
		hostIPs:      make(map[string]net.IP),
		EventChannel: make(chan Event, 10),
		readyChannel: make(chan struct{}),
	}
}

func (s *IpPoolService) Ready() bool {
	<-s.readyChannel
	return true
}

func (s *IpPoolService) HostIPs() []net.IP {
	s.lock.Lock()
	defer s.lock.Unlock()
	var ips []net.IP
	for _, ip := range s.hostIPs {
		ips = append(ips, ip)
	}
	return ips
}

func (s *IpPoolService) Loop() {
	if err := Execute(s.ctx, func() error {
		ips, err := s.ips()
		if err != nil {
			return nil
		}
		s.lock.Lock()
		defer s.lock.Unlock()

		var newIP = make(map[string]net.IP)
		for _, v := range ips {
			if v.To4() == nil {
				continue
			}
			_, ok := s.hostIPs[v.To4().String()]
			if ok {
				s.EventChannel <- Event{Type: UPDATE, IP: v.To4()}
			}
			if !ok {
				s.EventChannel <- Event{Type: ADD, IP: v.To4()}
			}
			newIP[v.To4().String()] = v.To4()
		}

		for k, v := range s.hostIPs {
			if _, ok := newIP[k]; !ok {
				s.EventChannel <- Event{Type: DEL, IP: v.To4()}
			}
		}

		s.hostIPs = newIP
		s.once.Do(func() {
			close(s.readyChannel)
		})
		return nil
	}, 5*time.Second); err != nil {

	}
}

func (s *IpPoolService) ips() ([]net.IP, error) {
	var ips []net.IP
	tables, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, table := range tables {
		if StringArrayContains(s.ignoreName, table.Name) {
			continue
		}
		adds, err := table.Addrs()
		if err != nil {
			return nil, err
		}
		for _, address := range adds {
			if inet := s.checkIPAddress(address); inet != nil {
				if inet.IP.To4() != nil {
					ips = append(ips, inet.IP.To4())
				}
			}
		}
	}
	return ips, nil
}

func (s *IpPoolService) checkIPAddress(addr net.Addr) *net.IPNet {
	ipnet, ok := addr.(*net.IPNet)
	if !ok {
		return nil
	}
	if ipnet.IP.IsLoopback() {
		return nil
	}
	return ipnet
}

func (s *IpPoolService) Watch() <-chan Event {
	return s.EventChannel
}

func (s *IpPoolService) Close() {
	s.cancel()
}

func StringArrayContains(list []string, source string) bool {
	for _, l := range list {
		if l == source {
			return true
		}
	}
	return false
}
