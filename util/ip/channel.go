/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package ip

import (
	"context"
	"time"
)

// SendNoBlocking is the Non-blocking send.
func SendNoBlocking(m []byte, ch chan []byte) {
	select {
	case ch <- m:
	default:
	}
}

// Interval is the intermittent execution.
func Interval(ctx context.Context, f func(), t time.Duration) {
	tick := time.NewTicker(t)
	defer tick.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-tick.C:
			f()
		}
	}
}

// Execute is the context execution.
func Execute(ctx context.Context, f func() error, wait time.Duration) error {
	timer := time.NewTimer(wait)
	defer timer.Stop()
	for {
		re := f()
		if re != nil {
			return re
		}
		timer.Reset(wait)
		select {
		case <-ctx.Done():
			return nil
		case <-timer.C:
		}
	}
}
